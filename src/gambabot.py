import asyncio
import json
import os
import re
import discord
from discord.ext import commands
from discord.ext.commands.core import command
from random import randrange

class GambaBot(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.last_text_channel = ""

    @commands.command(name = "dice", help = "Roll an N-sided dice")
    async def play(self, ctx, *args):
        sides = 999
        try:
            sides = int(" ".join(args))
        except:
            pass
        
        if sides > 1 and sides <= 999:
            await self.send_message(ctx, f"Random! (1-{sides}) 🎲{randrange(1, sides)}")
        else:
            await self.send_message(ctx, f"\"{sides}\" is not a valid setting.")

    async def send_message(self, ctx, message):
        self.last_text_channel = ctx.channel
        return await ctx.send(message)


def RunBot():
    COMMAND_PREFIX = os.getenv("COMMAND_PREFIX") or "!"

    bot = commands.Bot(command_prefix = COMMAND_PREFIX)
    gambabot = GambaBot(bot)
    bot.add_cog(gambabot)

    @bot.command(name = "ping", help = "Shows bot latency")
    async def ping(ctx):
        await ctx.send(f"Latency: {round(bot.latency * 1000)}ms")

    @bot.event
    async def on_ready():
        print("Bot Online")

        def event_handler(msg):
            if gambabot.last_text_channel:
                gambabot.last_text_channel.send(msg)

        await bot.change_presence(activity = discord.Activity(type = discord.ActivityType.listening, name = f"Music | {COMMAND_PREFIX}help"))
    
    bot.run(os.getenv("TOKEN"))

